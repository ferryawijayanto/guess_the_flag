//
//  GuessTheFlagController.swift
//  GuessTheFlag
//
//  Created by Ferry Adi Wijayanto on 19/11/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit

class GuessTheFlagController: UIViewController {
    
    var countries = [String]()
    var score = 0
    var correctAnswer = 0
    var askQuestion = 0
    
    let flagBtn1: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "us").withRenderingMode(.alwaysOriginal), for: .normal)
        btn.layer.borderWidth = 1
        btn.layer.borderColor = UIColor.lightGray.cgColor
        btn.tag = 0
        btn.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return btn
    }()
    
    let flagBtn2: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "us").withRenderingMode(.alwaysOriginal), for: .normal)
        btn.layer.borderWidth = 1
        btn.layer.borderColor = UIColor.lightGray.cgColor
        btn.tag = 1
        btn.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return btn
    }()
    
    let flagBtn3: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "us").withRenderingMode(.alwaysOriginal), for: .normal)
        btn.layer.borderWidth = 1
        btn.layer.borderColor = UIColor.lightGray.cgColor
        btn.tag = 2
        btn.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        countries += ["estonia", "france", "germany", "ireland", "italy", "monaco", "nigeria", "poland", "russia", "spain", "uk", "us"]
        
        
        setupViews()
        askQuestions()
    }
    
    func askQuestions(action: UIAlertAction! = nil) {
        countries.shuffle()
        correctAnswer = Int.random(in: 0...2)
        
        flagBtn1.setImage(UIImage(named: countries[0])?.withRenderingMode(.alwaysOriginal), for: .normal)
        flagBtn2.setImage(UIImage(named: countries[1])?.withRenderingMode(.alwaysOriginal), for: .normal)
        flagBtn3.setImage(UIImage(named: countries[2])?.withRenderingMode(.alwaysOriginal), for: .normal)
        
        title = countries[correctAnswer].uppercased()
        askQuestion += 1
    }
    
    @objc func buttonTapped(_ sender: UIButton) {
        var title: String
        
        if sender.tag == correctAnswer {
            title = "Correct"
            score += 1
        } else {
            title = "Wrong! That’s the flag of \(countries[sender.tag].uppercased())"
            score -= 1
        }
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Score: \(score) points", style: .plain, target: self, action: #selector(startNewGame))
        
        if askQuestion < 10 {
            let ac = UIAlertController(title: title, message: "Your score is \(score)", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "Continue", style: .default, handler: askQuestions))
            
            present(ac, animated: true, completion: nil)
            
        } else {
            let finalAC = UIAlertController(title: "Final Score", message: "Your final score is \(score)", preferredStyle: .alert)
            finalAC.addAction(UIAlertAction(title: "Close", style: .default, handler: startNewGame))
            
            present(finalAC, animated: true, completion: nil)
        }
    }
    
    @objc func startNewGame(action: UIAlertAction) {
        score = 0
        askQuestion = 0
        
        askQuestions()
    }
    
    func setupViews() {
        let stackViews = UIStackView(arrangedSubviews: [flagBtn1, flagBtn2, flagBtn3])
        stackViews.translatesAutoresizingMaskIntoConstraints = false
        stackViews.distribution = .fillEqually
        stackViews.axis = .vertical
        stackViews.spacing = 30
        
        view.addSubview(stackViews)
        
        NSLayoutConstraint.activate([
            stackViews.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            stackViews.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }
}
